angular.module('jobapp').controller("JobSearchController", JobSearchController);

function JobSearchController(JobFactory, $location) {
    const vm = this;
    vm.title = 'Job search';

    vm.input = '';
    vm.search = function () {
        JobFactory.search(vm.input).then(function (result) {
            vm.jobs = result;
        });
    }

    vm.update = function (id) {
        $location.url('/job-add?id=' + id);
    }

    vm.delete = function (id) {
        JobFactory.delete(id).then(function () {
            vm.search();
        });
    }
}