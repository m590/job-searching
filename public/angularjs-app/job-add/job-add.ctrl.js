angular.module('jobapp').controller("JobAddController", JobAddController);

function JobAddController(JobFactory, $location) {
    const vm = this;

    vm.newJob = {};
    vm.jobId = $location.$$search.id;
    if (vm.jobId) {
        vm.title = 'Update a job';
        JobFactory.get(vm.jobId).then(function (result) {
            vm.newJob = result;
            vm.newJob.skills = vm.newJob.skills.join(',');
        });
    } else {
        vm.title = 'Job add';
    }

    vm.saveGame = function () {
        if (vm.newJob.salary < 1000) {
            vm.err = "Salary must be greater than 1000";
        } else {
            if (vm.jobId) {
                JobFactory.update(vm.newJob).then(function (result) {
                    vm.err = '';
                    $location.url('/');
                });
            } else {
                JobFactory.add(vm.newJob).then(function (result) {
                    vm.err = '';
                    $location.url('/');
                });
            }
        }
    }


    vm.saveJob = function () {


    }
}