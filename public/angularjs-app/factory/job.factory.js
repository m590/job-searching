angular.module('jobapp').factory("JobFactory", JobFactory);

const _search_url = "/api/job-search?text="
const _url = "/api/jobs"

function JobFactory($http) {
    return {
        search: jobSearch,
        add: jobAdd,
        get: jobGet,
        update: jobUpdate,
        delete: jobDelete
    }

    function jobSearch(text) {
        return $http.get(_search_url + text).then(complete).catch(failure);
    }

    function jobGet(id) {
        return $http.get(_url + '/' + id).then(complete).catch(failure);
    }

    function jobUpdate(id) {
        return $http.put(_url + '/' + id).then(complete).catch(failure);
    }

    function jobAdd(job) {
        return $http.post(_url, job).then(complete).catch(failure);
    }

    function jobDelete(id) {
        return $http.delete(_url + '/' + id).then(complete).catch(failure);
    }

    function complete(response) {
        return response.data;
    }

    function failure(err) {
        return err;
    }
}