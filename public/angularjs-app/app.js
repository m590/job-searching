angular.module('jobapp', ['ngRoute'])
    .config(routeConfig);

function routeConfig($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: "/angularjs-app/job-search/job-search.html",
        controller: "JobSearchController",
        controllerAs: "vm"
    }).when("/job-add", {
        templateUrl: "/angularjs-app/job-add/job-add.html",
        controller: "JobAddController",
        controllerAs: "vm"
    }).when("/job-detail/:id", {
        templateUrl: "/angularjs-app/job-detail/job-detail.html",
        controller: "JobDetailController",
        controllerAs: "vm"
    });
}