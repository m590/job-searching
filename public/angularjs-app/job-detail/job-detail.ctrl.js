angular.module("jobapp").controller("JobDetailController", JobDetailController);

function JobDetailController(JobFactory, $routeParams) {
    const vm = this;

    console.log($routeParams.id);

    JobFactory.get($routeParams.id).then(function (result) {
        vm.job = result;
    });
}