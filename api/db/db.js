const mongoose = require('mongoose');
require('./job-model');

const dbUrl = process.env.dburl + process.env.dbName;

mongoose.connect(dbUrl, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

mongoose.connection.on('connected', function () {
    console.log('Mongoose connected to', dbUrl);
});

mongoose.connection.on('disconnected', function () {
    console.log('Mongoose disconnected from', dbUrl);
});

mongoose.connection.on('error', function () {
    console.log('Mongoose error', dbUrl);
});