const mongoose = require('mongoose');

const locationSchema = mongoose.Schema({
    street: String,
    city: String,
    state: String,
    zip: String,
    location: {
        "type": {
            type: String
        },
        coordinates: { // store coordinates in longitude (E/W) latitude (N/S)
            type: [Number],
            index: "2dsphere"
        }
    }
});

const jobSchema = mongoose.Schema({
    title: {type: String, required: true},
    salary: String,
    location: locationSchema,
    description: String,
    experience: String,
    skills: [String],
    postDate: Date
});

mongoose.model("Job", jobSchema, "jobs");