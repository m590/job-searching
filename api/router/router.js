const express = require('express');
const router = express.Router();

const jobCtrl = require('../controller/job.ctrl');

router.route("/jobs")
    .get(jobCtrl.getAll)
    .post(jobCtrl.addJob);

router.route("/job-search")
    .get(jobCtrl.search);

router.route("/jobs/:jobId")
    .get(jobCtrl.getOne)
    .delete(jobCtrl.deleteOne)
    .put(jobCtrl.updateJob);

module.exports = router;