const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Job = mongoose.model("Job");

module.exports.getAll = function (req, res) {
    let offset = 0, limit = 5;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }
    if (req.query && req.query.limit) {
        limit = parseInt(req.query.limit);
    }

    Job.find().skip(offset).limit(limit).exec(function (err, jobs) {
        const response = {
            status: 200,
            content: jobs
        }

        if (err) {
            console.log('JobCtrl.getAll error', err);
            response.status = 500;
            response.content = {message: "Internal error"};
        }

        res.status(response.status).json(response.content);
    });
}

module.exports.search = function (req, res) {
    let offset = 0, limit = 5;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }
    if (req.query && req.query.limit) {
        limit = parseInt(req.query.limit);
    }

    const query = {};
    if (req.query.text) {
        query.title = {
            $regex: req.query.text,
            $options: 'i'
        }
    }

    Job.find(query).skip(offset).limit(limit).exec(function (err, jobs) {
        const response = {
            status: 200,
            content: jobs
        }

        if (err) {
            console.log('JobCtrl.search error', err);
            response.status = 500;
            response.content = {message: "Internal error"};
        }

        res.status(response.status).json(response.content);
    });
}

module.exports.getOne = function (req, res) {
    const jobId = req.params.jobId;
    Job.findById(jobId).exec(function (err, job) {
        const response = {
            status: 200
        };

        if (job) {
            jwt.verify(job.salary, process.env.pass_phrase, function (err, decodedToken) {
                if (err) {
                    console.log('jwt salary verify error', err);
                } else {
                    job.salary = parseFloat(decodedToken.salary);
                    response.content = job;
                    res.status(response.status).json(response.content);
                }
            })
        } else {
            if (err) {
                console.log('jobCtrl.updateJob find jobById', err);
                response.status = 500;
                response.content = {message: "Internal error"};
            } else {
                response.status = 400;
                response.content = {message: "Job not found"};
            }
            res.status(response.status).json(response.content);
        }
    });
}

module.exports.addJob = function (req, res) {
    if (!req.body.title || !req.body.salary) {
        res.status(400).json({message: "Title and Salary must be"});
        return;
    }

    const response = {
        status: 500,
        content: {message: "Internal error"}
    };

    const newJob = {
        title: req.body.title,
        salary: jwt.sign({salary: req.body.salary}, process.env.pass_phrase, {}),
        description: req.body.description,
        experience: req.body.experience,
        postDate: new Date()
    }
    if (req.body.skills) {
        newJob.skills = req.body.skills.split(',');
    }

    const location = {};
    if (req.body.location) {
        location.street = req.body.location.street;
        location.city = req.body.location.city;
        location.state = req.body.location.state;
        location.zip = req.body.location.zip;

        if (req.body.location.lat && req.body.location.lng) {
            location.location = {};
            location.location.type = "Point";
            location.location.coordinates = [parseFloat(req.body.location.lng), parseFloat(req.body.location.lat)];
        }
    }

    newJob.location = location;

    Job.create(newJob, function (err, job) {
        if (err) {
            console.log('jobCtrl.addJob save job', err);
            response.status = 500;
            response.content = {message: "Internal error"};
        } else {
            response.status = 200;
            response.content = job;
        }
        res.status(response.status).json(response.content);
    });
}

const _updateJob = function (req, res, job) {
    const response = {
        status: 500,
        content: {message: "Internal error"}
    };

    job.title = req.body.title;
    job.salary = jwt.sign({salary: req.body.salary}, process.env.pass_phrase, {});
    job.description = req.body.description;
    job.experience = req.body.experience;
    if (req.body.skills) {
        job.skills = req.body.skills.split(',');
    }

    const location = {};
    if (req.body.location) {
        location.street = req.body.location.street;
        location.city = req.body.location.city;
        location.state = req.body.location.state;
        location.zip = req.body.location.zip;

        if (req.body.location.lat && req.body.location.lng) {
            location.location = {};
            location.location.type = "Point";
            location.location.coordinates = [parseFloat(req.body.location.lng), parseFloat(req.body.location.lat)];
        }
    }
    job.location = location;

    job.save(function (err, job) {
        if (err) {
            console.log('jobCtrl.updateJob save job', err);
            response.status = 500;
            response.content = "Internal error";
        } else {
            response.status = 200;
            response.content = job;
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.updateJob = function (req, res) {
    if (!req.body.title || !req.body.salary) {
        res.status(400).json({message: "Title and Salary must be"});
        return;
    }

    const jobId = req.params.jobId;
    Job.findById(jobId).exec(function (err, job) {
        const response = {
            status: 200
        };

        if (job) {
            _updateJob(req, res, job);
        } else {
            if (err) {
                console.log('jobCtrl.updateJob find jobById', err);
                response.status = 500;
                response.content = {message: "Internal error"};
            } else {
                response.status = 400;
                response.content = {message: "Job not found"};
            }
            res.status(response.status).json(response.content);
        }
    });
}

module.exports.deleteOne = function (req, res) {
    const jobId = req.params.jobId;
    Job.findByIdAndDelete(jobId).exec(function (err, job) {
        const response = {
            status: 200
        };

        if (job) {
            response.content = job;
        } else {
            if (err) {
                console.log('jobCtrl.updateJob find jobById', err);
                response.status = 500;
                response.content = {message: "Internal error"};
            } else {
                response.status = 400;
                response.content = {message: "Job not found"};
            }
        }
        res.status(response.status).json(response.content);
    });
}